const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const extractPlugin = new ExtractTextPlugin({
  filename: '../style.css',
  // filename: (getPath) => {
  //   return getPath('css/[name].css').replace('css/js', '../');
  // },
  allChunks: true
});

const uglifyJS = new UglifyJsPlugin({
  test: /\.js($|\?)/,
  sourceMap: true,
  uglifyOptions: {
    ecma: 6
  }
});

const config = {
  entry: './src/app.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/dist'
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['es2015'],
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /\.styl$/,
        // use: extractPlugin.extract(['css-loader', 'postcss-loader', 'stylus-loader'])
        use: extractPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: { importLoaders: 1, sourceMap: true }
            },
            {
              loader: 'postcss-loader', options: { sourceMap: true }
            },
            {
              loader: 'stylus-loader', options: { sourceMap: true }
            }
          ]
        })
      }
    ]
  },
  plugins: [
    extractPlugin,
    uglifyJS
  ]
};

module.exports = config;